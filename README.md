### Informacion
Proyecto final del bloque de JavaScript. 
Aplicación donde se refuerzan los conocimientos impartidos en clase, funciones, condiciones, bucles, recorrer objetos, promesas, peticiones FETCH a APIS publicas.

En mi caso la API es pública pero con autenticación por TOKEN. El token está definido en una variable.

Listado de peliculas > Muestra 10.000 películas, por popularidad actual. Muestra 20 películas por página, con total de 500. 

Botón random > Muestra una película al azar de las 20 que te da, y entre las 500 paginas.

### Comandos:

Install dependencies (only first time):
> npm install

Dev mode:
> npm run dev
