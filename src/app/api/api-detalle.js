
const cargarPeliculaDetalle = async () => { // (EVENT)
    const token = '75eb9d7eba61e749bdf9a26131cdc188';
    const idioma = 'language=es-ES';
    const id = 254;
    try {
        const respuesta = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${token}&${idioma}`);
        const peliculaDetalle = await respuesta.json();
        if (respuesta.status === 200) { // Si la respuesta es OK entra en este IF
            let pelicula = peliculaDetalle;
             return pelicula;
            }
            else if (respuesta.status === 401) {
                console.log('Revisa tu token, el actual no es correcto');
            }
            else if (respuesta.status === 404) {
                console.log('La web no está disponible');
            }
            else console.log('Error desconocido. Consulte la documentación para mas información');
        } catch (error) {
        console.log(error)
    }
}

export { cargarPeliculaDetalle }; // return pelicula
