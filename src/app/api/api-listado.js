import { listarPeliculas } from '../vista/vista-listado'
var listadoPeliculas = [];

let pagina = 1;

const btnNext = () => {
    if ((pagina > 1) || (pagina < 500)) {
        pagina++;
        listarPeliculas();
        } else {
            window.alert(`No hay mas paginas disponibles`)
        }
    }

const btnBack = () => {
    if ((pagina <= 1) || (pagina < 500)) {
        pagina--;
        listarPeliculas();
        } else {
            window.alert(`Estas en la primera página`)
        }
    }

// CARGA DE DATOS CON FETCH
const cargarPeliculasListado = async () => {

    const token = '75eb9d7eba61e749bdf9a26131cdc188';
    const idioma = 'language=es-ES';
    try {
        const respuesta = await fetch(`https://api.themoviedb.org/3/movie/popular/?api_key=${token}&${idioma}&page=${pagina}`);
        const datos = await respuesta.json();
        // console.log(respuesta)
        // console.log(datos)
        if (respuesta.status === 200) { // Si la respuesta es OK entra en este IF
            listadoPeliculas = datos.results;

             return listadoPeliculas;
            }
            else if (respuesta.status === 401) {
                console.log('Revisa tu token, el actual no es correcto');
            }
            else if (respuesta.status === 404) {
                console.log('La web no está disponible');
            }
            else console.log('Error desconocido. Consulte la documentacion para mas información');
        } catch (error) {
        console.log(error)
    }

}

export { cargarPeliculasListado, btnNext, btnBack };



// RETURN DE cargarPeliculasListado -> return listadoPeliculas