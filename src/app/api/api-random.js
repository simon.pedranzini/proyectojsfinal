    let min = 1;
    let max = 500;
    const idioma = 'language=es-ES';

    const recorrerPaginasRandom = async () => {
        const token = '75eb9d7eba61e749bdf9a26131cdc188';
        const page = Math.round(Math.random() * (500 - 1) + 1);
    try {
        const datos = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${token}&${idioma}&page=${page}`);
        let res = await datos.json();
        console.log(res)
        return res;
    } catch (error) {
        console.log(error);
    }
}

export { recorrerPaginasRandom }; // DEVUELVE RES