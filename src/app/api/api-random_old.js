
const buscarIdPublico = async () => {
    const token = '75eb9d7eba61e749bdf9a26131cdc188';
    const idioma = 'language=es-ES';
    const id = Math.round(Math.random() * (1000 - 1) + 1);
    let films;
    try {
        const urlIdExterno = await fetch(`https://api.themoviedb.org/3/movie/${id}/external_ids?api_key=${token}`);
        let idExtern = await urlIdExterno.json();
        idExtern = idExtern.imdb_id;
        console.log(idExtern)
        if ((idExtern !== 'null') || (idExtern !== undefined)) {
        const buscarPelicula = await fetch(`https://api.themoviedb.org/3/find/${idExtern}?api_key=${token}&${idioma}&external_source=imdb_id`)
        let idIntern = await buscarPelicula.json();
        console.log(idIntern)
        let pelicula = idIntern.movie_results;
            for (const key in pelicula ) {
                films = pelicula[key]
            }
        return films;
        } else { console.error('ERROR, la película no tiene imagen')}
    } catch (error) {
        console.log(error);
    }
}

export { buscarIdPublico };

