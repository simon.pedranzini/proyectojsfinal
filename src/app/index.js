import { ocultarListado, listarPeliculas } from './vista/vista-listado'
import { cargarPeliculaRandom } from './vista/vista-random'
import { btnNext, btnBack } from './api/api-listado';

import './styles/styles.scss';
import 'bootstrap';

const addListeners = () => {
  document.getElementById('btnListado').addEventListener('click', ocultarListado)
  document.getElementById('btnListado').addEventListener('click', listarPeliculas)
  document.getElementById('btnAzar').addEventListener('click',   cargarPeliculaRandom)
  document.getElementById('btnNext').addEventListener('click',   btnNext)
  document.getElementById('btnBack').addEventListener('click',   btnBack)
  // document.getElementById('masInfo').addEventListener('click',   botonInformacion)
  //document.getElementById('vistaDetallada').addEventListener('click', listarPeliculas )
}

window.onload = () => {
  addListeners();
};
