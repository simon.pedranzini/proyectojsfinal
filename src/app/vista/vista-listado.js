import {
    cargarPeliculasListado
} from '../api/api-listado' // RETURN DE cargarPeliculasListado -> return listadoPeliculas

const listarPeliculas = (event) => {

    const peliculasListadoPromesa = cargarPeliculasListado();

    peliculasListadoPromesa.then((listadoPeliculas) => {
        let peliculas = '';
        generalDetalles.innerHTML = '';
        // let mediaPuntuacion;
        listadoPeliculas.forEach((element) => {
            // mediaPuntuacion = (element.vote_average * 10)
            // console.log(mediaPuntuacion)
            peliculas += `
        <div class="containerPelicula">
        <p class="infoPelicula infoTitulo">${element.title}</p>
        <p class="infoPelicula">Fecha estreno: ${element.release_date}</p>
        <p class="infoPelicula">Puntuación: ${element.vote_average}</p>
        <button class="botonInfo">Ver más...</button>
        </div>
        `;
    })

    ;
    document.getElementById('containerListPeliculas').innerHTML = peliculas;
    // document.querySelector(`.${mediaPuntuacion} .stars-inner`).style.width = mediaPuntuacion
    return peliculas;
    })
}

const ocultarListado = () => {
    var ocultarListado = document.getElementById('containerListPeliculas');
    ocultarListado.classList.toggle('hide');
}


export {
    listarPeliculas,
    ocultarListado
}




{/* <span class="infoPelicula stars-outer"><span class="stars-inner"></span></span> */}