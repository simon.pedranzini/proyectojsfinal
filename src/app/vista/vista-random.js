import {
  recorrerPaginasRandom
} from '../api/api-random' // DEVUELVE RES

const cargarPeliculaRandom = () => { // (EVENT)

  const datosPromesa = recorrerPaginasRandom();
  let datos, element, resultado;
  let noImage = '<img src="../assets/no-image.png" alt="Imagen vacía">'
  datosPromesa.then((res) => {
    for (const key in res) {
      datos = res.results
    }
    element = Math.round(Math.random() * (20 - 1) + 1);
    containerListPeliculas.innerHTML = '';
    const urlImagenes = `https://image.tmdb.org/t/p/w500/${datos[element].poster_path}`
    const noImage = `<img src="../assets/no-image.png" src="No portada">`
    if ((datos.poster_path !== null) || (datos.poster_path !== undefined)) {
      resultado += `
      <div class="containerDetallePelicula" class="card mb-3 max-weigh:769px">
        <div class="row g-0">
          <div class="col-md-2">
            <img src="${urlImagenes}" class="imgPoster" alt="Cartel película">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">${datos[element].title}</h5>
              <p class="card-text">${datos[element].overview}</p>
              <p class="card-text"><small class="text-muted">${datos[element].release_date}</small></p>
            </div>
          </div>
        </div>
      </div>
      `
    } else {
      resultado += `
          <div class="containerDetallePelicula" class="card mb-3 max-weigh:769px">
        <div class="row g-0">
          <div class="col-md-2">
            <img src="${noImage}" class="imgPoster" alt="Cartel película">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">${datos[element].title}</h5>
              <p class="card-text">${datos[element].overview}</p>
              <p class="card-text"><small class="text-muted">
              ${noImage}
              </p>
            </div>
          </div>
        </div>
      </div>
            `
    };
    document.getElementById('generalDetalles').innerHTML = resultado;
  });
  return resultado;
}

export {
  cargarPeliculaRandom
};


{
  /* <div class="containerDetallePelicula">
  <img class="imgPoster" src='${urlImagenes}'>
  <div class="infoPelicula">
  <p class="tiluloPelicula">${datos[element].title}</p>
  <p class="descripcionPelicula">${datos[element].overview}</p>
  <p class="fechaPelicula">${datos[element].release_date}</p>
  </div>
  </div> */
}